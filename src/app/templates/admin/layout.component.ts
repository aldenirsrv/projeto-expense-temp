/*** Angular imports  */
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Renderer2
} from '@angular/core';
import 'rxjs/add/operator/filter';

import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
/*** Interfaces imports */
import { Menu } from './layout.interface';
import { UserService } from '../../../singleton/guard/user.service';
import { TranslateService } from '../../shared/components/translate';
/** Services */

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styles: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  public menuItens: Array<Menu>;
  public activeRoute: string;
  private selectedRoute: number;
  private pageW: number;
  private vp: number;
  public supportedLanguages;
  public currentlanguage;
  @ViewChild('mobileNav') div: ElementRef;
  constructor(
    private user: UserService,
    private router: Router,
    private renderer: Renderer2,
    private activatedRoute: ActivatedRoute,
    public _translate: TranslateService
  ) {
    // this.getActivedRoute();
    this.menuItens = [
      {
        name: 'Home',
        // icon: 'fa fa-home',
        icon: 'home',
        class: 'c-blue-500',
        route: '/home'
      },
      {
        name: 'Dashboard',
        // icon: 'fa fa-bar-chart',
        icon: 'equalizer',
        class: 'c-blue-500',
        route: '/admin/dashboard'
      },
      {
        name: 'Empresas',
        // icon: 'fa fa-building',
        icon: 'domain',
        class: 'c-brown-500',
        route: '/admin/companies'
      },
      {
        name: 'Usuários',
        icon: 'group',
        class: 'c-blue-500',
        route: '/admin/users'
      },
      {
        name: 'Aprovação',
        // icon: 'fa fa-share-alt-square',
        icon: 'done_all',
        class: 'c-deep-orange-500',
        route: '/admin/approvals'
      },
      {
        name: 'Relatórios',
        // icon: 'fa fa-folder',
        icon: 'assignment',
        class: 'c-deep-purple-500',
        route: '/admin/reports'
      },
      {
        name: 'Faturamento',
        // icon: 'fa fa-money',
        icon: 'trending_up',
        class: 'c-deep-purple-500',
        route: '/admin/billings'
      },
      {
        name: 'Categorias',
        // icon: 'fa fa-money',
        icon: 'label',
        class: 'c-deep-purple-500',
        route: '/admin/categories'
      }
    ];
    /* this.router.events.pairwise().subscribe(event => {
      window.scrollTo(0, 0);
      console.log(event[0]);
      console.log(event[1]);
    });
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        const ele = document.getElementById('processe-list');
        if (ele) {
          ele.className = 'processe-list leave-page';
          ele.className = 'full-container leaving-page';
          setTimeout(() => {
            ele.className = 'full-container entring-page';
          }, 100);
        }
      });*/
  }
  public ngOnInit() {
    this.pageW = window.innerWidth * 0.7;
    this.sidebar();
    this.searchToggle();
    this.dropdownList();
    // standing data
    this.vp = document.documentElement.clientHeight;
    console.log(document.querySelectorAll('.app'));
    this.renderer.removeClass(
      document.getElementById('app-root'),
      'is-collapsed'
    );

    console.log(
      document.getElementById('app-root').classList.contains('is-collapsed')
    );
    this.supportedLanguages = [
      { display: 'PT (Br)', value: 'ptbr', icon: 'assets/images/icons/brazil.svg' },
      { display: 'English', value: 'en', icon: 'assets/images/icons/usa.svg'  },
      { display: 'Español', value: 'es', icon: 'assets/images/icons/spain.svg'  },
    ];
    // set current langage
    this.selectLang('ptbr');
    // console.log(<HTMLElement>document.getElementById('app-root').toggleClass('teste'));
  }

  selectLang(lang: string) {
    this._translate.use(lang);
    this.currentlanguage = this._translate.currentLang;
  }
  ngAfterViewInit() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((event: NavigationEnd) => {
        this.navegateTo(event);
      });
    /*const links = document.getElementsByClassName('mobile-nav__item');
    console.log(links.classList.contains('calendar-row'));
    cothis.div.nativeElement.find('.mobile-nav__item');
    console.log(this.div.nativeElement);*/
  }
  sidebar() {
    const sideActive = document
      .getElementById('app-root')
      .classList.contains('is-collapsed');

    if (sideActive && sideActive) {
      this.renderer.removeClass(
        document.getElementById('app-root'),
        'is-collapsed'
      );
    } else {
      this.renderer.addClass(document.getElementById('app-root'), 'is-collapsed');
    }
    // document.getElementsByClassName('.app')[0].toggleClass('is-collapsed');
    // Sidebar links
    /*
    $('.sidebar .sidebar-menu li a').on('click', function() {
      const $this = $(this);

      if ($this.parent().hasClass('open')) {
        $this
          .parent()
          .children('.dropdown-menu')
          .slideUp(200, () => {
            $this.parent().removeClass('open');
          });
      } else {
        $this
          .parent()
          .parent()
          .children('li.open')
          .children('.dropdown-menu')
          .slideUp(200);

        $this
          .parent()
          .parent()
          .children('li.open')
          .children('a')
          .removeClass('open');

        $this
          .parent()
          .parent()
          .children('li.open')
          .removeClass('open');

        $this
          .parent()
          .children('.dropdown-menu')
          .slideDown(200, () => {
            $this.parent().addClass('open');
          });
      }
    });
    */

    // Sidebar Activity Class
    const sidebarLinks = [];
    // $('.sidebar').find('.sidebar-link');

    /* sidebarLinks
     .each((index, el) => {
       $(el).removeClass('active');
      })
      .filter(function() {
        const href = $(this).attr('href');
        const pattern = href[0] === '/' ? href.substr(1) : href;
        return pattern === window.location.pathname.substr(1);
      })
      .addClass('active'); */

    // ٍSidebar Toggle
    /* $('.sidebar-toggle').on('click', e => {
      $('.app').toggleClass('is-collapsed');
      e.preventDefault();
    });*/

    /**
     * Wait untill sidebar fully toggled (animated in/out)
     * then trigger window resize event in order to recalculate
     * masonry layout widths and gutters.
     */
    /*$('#sidebar-toggle').click(e => {
      e.preventDefault();
      setTimeout(() => {
        window.dispatchEvent(window.event);
      }, 300);
    });*/
  }
  searchToggle() {
    /* $('.search-toggle').on('click', e => {
      $('.search-box, .search-input').toggleClass('active');
      $('.search-input input').focus();
      e.preventDefault();
    }); */
  }

  dropdownList() {
    return [
      {
        name: 'Configuraõces',
        icon: 'ti-settings',
        separe: false,
        action: e => {
          console.log(e);
        }
      },
      {
        name: 'Perfil',
        icon: 'ti-user',
        separe: false,
        action: e => {
          console.log(e);
        }
      },
      {
        name: 'Mensagens',
        icon: 'ti-email',
        separe: true,
        action: e => {
          console.log(e);
        }
      },
      {
        name: 'Sair',
        separe: false,
        icon: 'ti-power-off',
        action: e => {
          console.log('sair');
          this.user.logout();
        }
      }
    ];
  }

  navegateTo = event => {
    const scrollSize = this.pageW / 3;

    this.menuItens.forEach((item, x) => {
      if (item.route === event.url) {
        if (x > 1) {
          this.div.nativeElement.scrollTo({
            left: scrollSize * (x - 1),
            top: 0,
            behavior: 'smooth'
          });
        }
        if (x <= 1) {
          this.div.nativeElement.scrollTo({
            left: 0,
            top: 0,
            behavior: 'smooth'
          });
        }
      }
    });
  }
  getPage = outlet => {
    return outlet.activatedRouteData.state;
  }
}
