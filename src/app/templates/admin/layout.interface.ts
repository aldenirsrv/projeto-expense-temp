
export interface Menu {
  name: string;
  icon: string;
  class?: string;
  route?: string;
  itens?: Array<Menu>;
}
