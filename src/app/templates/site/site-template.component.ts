import { Component, OnInit, HostListener, ElementRef, AfterViewInit } from '@angular/core';
import { TranslateService } from '../../shared/components/translate';

@Component({
  selector: 'app-site-template',
  templateUrl: './site-template.component.html',
  styleUrls: ['./site-template.component.scss']
})
export class SiteTemplateComponent implements OnInit, AfterViewInit {
  public isFixed = false;
  public supportedLanguages;
  public currentlanguage;
  constructor(private el: ElementRef,  public _translate: TranslateService) { }

  ngOnInit() {
    this.supportedLanguages = [
      { display: 'PT (Br)', value: 'ptbr', icon: 'assets/images/icons/brazil.svg' },
      { display: 'English', value: 'en', icon: 'assets/images/icons/usa.svg'  },
      { display: 'Español', value: 'es', icon: 'assets/images/icons/spain.svg'  },
    ];
    // set current langage
    this.selectLang('es');
  }
  selectLang(lang: string) {
    this._translate.use(lang);
    this.currentlanguage = this._translate.currentLang;
  }
  onScroll = (event) => {
    console.log(event);
    console.log(window.screenTop);
    // let win = $(window);
  }
  @HostListener('window:scroll', ['$event'])
    onWindowScroll(event) {
      if (window.pageYOffset >= 250) {
       this.isFixed = true;
      } else {
        this.isFixed = false;
      }
    }
    ngAfterViewInit() {
    }
    /*
    navegateTo = event => {
      const scrollSize = this.pageW / 3;
      this.menuItens.forEach((item, x) => {
        if (item.route === event.url) {
          if (x > 1) {
            this.div.nativeElement.scrollTo({
              left: scrollSize * (x - 1),
              top: 0,
              behavior: 'smooth'
            });
          }
          if (x <= 1) {
            this.div.nativeElement.scrollTo({
              left: 0,
              top: 0,
              behavior: 'smooth'
            });
          }
        }
      });
    }
    */
}
