import { Directive, AfterViewInit, OnDestroy, OnInit, EventEmitter, ElementRef, HostListener
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/startWith';

@Directive({
    selector: '[appToggleLeftBar]'
})
export class ToogleLeftBarDirective implements OnInit, AfterViewInit, OnDestroy {
    public el;
    public bt;
    constructor() {}
    ngOnInit() {
        this.el = document.getElementById('collapsed-content') as HTMLElement;
        this.bt  = document.getElementById('left-bar-toggle-button') as HTMLElement;
        this.checkWindowSize(window.innerWidth);
    }
    ngAfterViewInit() { }
    @HostListener('click') mouseleave() {
        if (this.el.classList.contains('is-collapsed')) {
            this.el.classList.remove('is-collapsed');
            this.bt.classList.remove('btn-is-collapsed');
        } else {
            this.el.classList.add('is-collapsed');
            this.bt.classList.add('btn-is-collapsed');
        }
    }
    @HostListener('window:resize', ['$event'])
        onResize(event) {
            if ( event.target.innerWidth < 768 ) {
                this.el.classList.remove('is-collapsed');
                this.bt.classList.remove('btn-is-collapsed');
            } else {
                this.el.classList.add('is-collapsed');
                this.bt.classList.add('btn-is-collapsed');
            }
        }
    checkWindowSize = (size) => {
        if ( size < 768 ) {
            this.el.classList.remove('is-collapsed');
            this.bt.classList.remove('btn-is-collapsed');
        } else {
            this.el.classList.add('is-collapsed');
            this.bt.classList.add('btn-is-collapsed');
        }
    }
    ngOnDestroy() { }
}
