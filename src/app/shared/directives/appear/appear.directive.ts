import {
    ElementRef,
    Output,
    Directive,
    AfterViewInit,
    OnDestroy,
    EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/startWith';

@Directive({
    selector: '[appAppear]'
})
export class AppearDirective implements AfterViewInit, OnDestroy {
    @Output() appear: EventEmitter<boolean>;

    elementPos: number;
    elementHeight: number;

    scrollPos: number;
    windowHeight: number;

    subscriptionScroll: Subscription;
    subscriptionResize: Subscription;

    constructor(private element: ElementRef) {
        this.appear = new EventEmitter<boolean>();
    }

    saveDimensions() {
        this.elementPos = this.getOffsetTop(this.element.nativeElement);
        this.elementHeight = this.element.nativeElement.offsetHeight;
        this.windowHeight = window.innerHeight;
    }
    saveScrollPos() {
        this.scrollPos = window.scrollY;
        // console.log(this.scrollPos);
    }
    getOffsetTop(element: any) {
        let offsetTop = element.offsetTop || 0;
        if (element.offsetParent) {
            offsetTop += this.getOffsetTop(element.offsetParent);
        }
        return offsetTop;
    }
    checkVisibility() {
        // console.log(this.isVisible());
        // if (this.isVisible()) {
            // double check dimensions (due to async loaded contents, e.g. images)
            this.saveDimensions();
            this.appear.emit(this.isVisible());
        // }
    }
    isVisible() {
        /**
         * check element enter to the view.
         * if check element completaly enter to view
         * add + this.elementHeight in finish of operation ... this.elementPos + this.elementHeight
         *
         */
        // Element position enter in view
        const elementEnter =  this.elementPos - this.windowHeight;

        // Element position leave in view
        const elementLeave =   this.elementPos + this.elementHeight;
        return (this.scrollPos >= elementEnter && this.scrollPos <= elementLeave);
        /*return (
            this.scrollPos >= this.elementPos ||
            this.scrollPos + this.windowHeight >= this.elementPos
        );*/
    }

    subscribe() {
        this.subscriptionScroll = Observable.fromEvent(window, 'scroll')
            .startWith(null)
            .subscribe(() => {
                this.saveScrollPos();
                this.checkVisibility();
            });
        this.subscriptionResize = Observable.fromEvent(window, 'resize')
            .startWith(null)
            .subscribe(() => {
                this.saveDimensions();
                this.checkVisibility();
            });
    }
    unsubscribe() {
        if (this.subscriptionScroll) {
            this.subscriptionScroll.unsubscribe();
        }
        if (this.subscriptionResize) {
            this.subscriptionResize.unsubscribe();
        }
    }

    ngAfterViewInit() {
        this.subscribe();
    }
    ngOnDestroy() {
        this.unsubscribe();
    }
}
