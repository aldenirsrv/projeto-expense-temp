import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';

/**
 * Thirty party
 */
import { MaskModule } from 'soft-angular-mask';
import { ChartModule } from 'angular-highcharts';
/**
 * Shared Services
 */

import { ModalService } from './components/modal/modal.service';
import { SidebarService } from './components/sidebar/sidebar.service';
import { DomService } from './components/dom.service';

/**
 * Shared pipes
 */

/**
 * Custom shared components
 */
// import { AppSettings } from '../app.constants';
import { HighchartsCustomComponentComponent } from './components/highcharts-custom-component/highcharts-custom-component.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { ErrorsComponent } from './components/input-errors/erros.component';
import { ConfirmDialogComponent } from './components/dialogs/confirm/confirm.dialog';
import { DialogComponent } from './components/dialogs/dialog.component';

import {DemoMaterialModule} from './../material.module';
import { PaginationComponent } from './components/pagination/pagination.component';


/**
 * Services http
 */
import { BaseService } from '../../singleton/http/base.service';
import { HttpService } from '../../singleton/http/http.service';
import { HelperService } from '../../singleton/http/helper.service';
import { CustomErrorHandlerService } from '../../singleton/http/custom-error-handler.service';
import { TostNotificationService } from '../../singleton/notification/toast-notification.service';
import { CustomSuccessHandlerService } from '../../singleton/http/custom-success-handler.service';
import { UserService } from '../../singleton/guard/user.service';
import { LoggedInGuard } from '../../singleton/guard/loggedIn-guard.service';
import { UsersFilterComponent } from '../pages/users/users-filter/users-filter.component';
import { TranslatePipe } from './components/translate';

/**
 * Directives
 */

import { EqualValidator } from './components/input-errors/equal-validator.directive';
import { MinValidatorDirective } from './components/input-errors/validators/min-value-validator.directive';
import { TelephoneNumberFormatValidatorDirective } from './components/input-errors/validators/telephone-number-validator.directive';
import { BirthYearValidatorDirective } from './components/input-errors/validators/birth-year-validator.directive';
import { AppearDirective } from './directives/appear/appear.directive';
import { ToogleLeftBarDirective } from './directives/left-bar/left-bar.directive';
import { TooltipDirective } from './directives/tooltip/tooltip.directive';


import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { SideNavComponent } from './components/side-nav/side-nav.component';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    // override hammerjs default configuration
    'pan': { threshold: 5, velocity: 0.2 },
    'swipe': {
      velocity: 0.2,
      threshold: 20,
      direction: 31 // /!\ ugly hack to allow swipe in all direction
    }
  };
}
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    HttpModule,
    MaskModule,
    ChartModule,
    DemoMaterialModule,
    PerfectScrollbarModule
  ],
  providers: [
    { provide: Http, useClass: HttpService },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    /*{
      provide: Http,
      useFactory: (backend: XHRBackend, options: RequestOptions) => {
        return new HttpService(backend, options);
      },
      deps: [XHRBackend, RequestOptions]
    }
    /* {
      provide: HttpService,
      useFactory: (backend: XHRBackend, options: RequestOptions) => {
        return new HttpService(backend, options);
      },
      deps: [XHRBackend, RequestOptions]
    }*/

    BaseService,
    HelperService,
    CustomErrorHandlerService,
    CustomSuccessHandlerService,
    TostNotificationService,
    HttpService,
    DomService,
    UserService,
    ModalService,
    SidebarService,
    LoggedInGuard
  ],
  declarations: [
    /** components */
    HighchartsCustomComponentComponent,
    CalendarComponent,
    ScheduleComponent,
    ConfirmDialogComponent,
    DialogComponent,
    UsersFilterComponent,
    ErrorsComponent,
    PaginationComponent,
    SideNavComponent,
    // AppSettings
    /** pipes */
    TranslatePipe,
    /** Directives */
    EqualValidator,
    MinValidatorDirective,
    TelephoneNumberFormatValidatorDirective,
    BirthYearValidatorDirective,
    AppearDirective,
    ToogleLeftBarDirective,
    TooltipDirective
  ],
  exports: [
    FormsModule,
    /** Thrity party */
    MaskModule,
    ChartModule,

    /** components */
    HighchartsCustomComponentComponent,
    CalendarComponent,
    ScheduleComponent,
    ConfirmDialogComponent,
    DialogComponent,
    UsersFilterComponent,
    ErrorsComponent,
    PaginationComponent,
    SideNavComponent,
    // AppSettings,
    /** pipes */
    TranslatePipe,


    /** modules */
    HttpClientModule,
    HttpModule,
    DemoMaterialModule,
    PerfectScrollbarModule,

    /** Directives */
    EqualValidator,
    MinValidatorDirective,
    BirthYearValidatorDirective,
    AppearDirective,
    ToogleLeftBarDirective,
    TooltipDirective
  ],
  entryComponents: [UsersFilterComponent, ConfirmDialogComponent]
})
export class SharedModule { }
