// https://itnext.io/angular-create-your-own-modal-boxes-20bb663084a1
import { Injectable } from '@angular/core';
import { DomService } from '../dom.service';

@Injectable()
export class SidebarService {
  constructor(private domService: DomService) {}

  private sidebarElementId = 'sidebar-container';
  private overlayElementId = 'overlay-sidebar';

  init(component: any, inputs: object, outputs: object) {
    const componentConfig = {
      inputs: inputs,
      outputs: outputs
    };
    this.domService.appendComponentTo(
      this.sidebarElementId,
      component,
      componentConfig
    );
    document.getElementById(this.sidebarElementId).className = 'sidebar-show';
    document.getElementById(this.overlayElementId).className = 'show-sidebar-overlay';
    document.getElementById(this.overlayElementId).className = 'show-sidebar-overlay';
    console.log("#deve abrir o sidebar");
  }

  destroy() {
    // ;
    document.getElementById(this.sidebarElementId).className = 'sidebar-hidden';
    document.getElementById(this.overlayElementId).className = 'hidden-sidebar-overlay';
    setTimeout(() => {
      this.domService.removeComponent();
    }, 400);
  }
}
