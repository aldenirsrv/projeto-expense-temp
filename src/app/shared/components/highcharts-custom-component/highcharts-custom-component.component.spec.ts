import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighchartsCustomComponentComponent } from './highcharts-custom-component.component';

describe('HighchartsCustomComponentComponent', () => {
  let component: HighchartsCustomComponentComponent;
  let fixture: ComponentFixture<HighchartsCustomComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighchartsCustomComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighchartsCustomComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
