import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';
@Component({
  selector: 'app-highcharts-custom-component',
  template: '<div [chart]="chart"></div>',
  styleUrls: ['./highcharts-custom-component.component.scss']
})
export class HighchartsCustomComponentComponent implements OnInit {
  @Input() data: Array<any>;
  @Input() type: string;
  @Input() chartHeight: string;
  public chart;
  public colors = ['#4caf50', '#FD5F00', '#4c84ff', '#ff5560', '#d50000', '#c51162', '#aa00ff', '#6200ea', '#0091ea', '#00bfa5', '#00c853', '#ffab00'];

  constructor() { }

  ngOnInit() {

    this.chart = new Chart({
      chart: {
        type: this.type ? this.type : 'column'
      },
      title: {
        text: ' '
      },
      colors: this.colors,
      credits: {
        enabled: false
      },
      series: this.data ? this.data : []
    });
  }

}
