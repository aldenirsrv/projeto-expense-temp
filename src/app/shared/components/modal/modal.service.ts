// https://itnext.io/angular-create-your-own-modal-boxes-20bb663084a1
import { Injectable } from '@angular/core';
import { DomService } from '../dom.service';

@Injectable()
export class ModalService {
  constructor(private domService: DomService) {}

  private modalElementId = 'modal-container';
  private overlayElementId = 'overlay';

  init(component: any, inputs: object, outputs: object, position?: string) {
    const componentConfig = {
      inputs: inputs,
      outputs: outputs
    };
    this.domService.appendComponentTo(
      this.modalElementId,
      component,
      componentConfig
    );
    document.getElementById(this.modalElementId).className = 'show';
    document.getElementById(this.overlayElementId).className = 'show-overlay';
    document.getElementById(this.overlayElementId).className = 'show-overlay';
    this.modalPosition(position);
    console.log("#deve abrir o modal");
  }

  destroy() {
    // ;
    document.getElementById(this.modalElementId).className = 'hidden';
    document.getElementById(this.overlayElementId).className = 'hidden-overlay';
    setTimeout(() => {
      this.domService.removeComponent();
    }, 400);
  }
  modalPosition(pos) {
    switch (pos) {
      case 'center':
        document.getElementById(this.modalElementId).style.alignItems =
          'center';
        break;
      case 'top':
        document.getElementById(this.modalElementId).style.alignItems =
          'flex-start';
        break;
      case 'bottom':
        document.getElementById(this.modalElementId).style.alignItems =
          'flex-end';
        break;
      default:
        document.getElementById(this.modalElementId).style.alignItems =
          'flex-end';
    }
  }
}
