import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
/** show/hiden modal component */
@Input() showCustomModal: boolean;
/** output boolean false to father when modal close */
@Output() sidenavClosed: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

  ngOnInit() {
   // this.showCustomModal = true;
  }

  /**
   * Button close the modal
   */
  closeModal = () => {
    this.showCustomModal = false;
    this.sidenavClosed.emit(this.showCustomModal);
  }

}
