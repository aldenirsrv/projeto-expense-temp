How use translate service in angular 2+
===================
----------
#### Import translate service to your app.module.ts
> *import { TranslateService, TRANSLATION_PROVIDERS} from './shared/components/translate';*
##-##


#### Update your *provider* in app.module.ts
> *providers: [TRANSLATION_PROVIDERS, TranslateService, { provide: 'SnotifyToastConfig', useValue: ToastDefaults}, SnotifyService]...*


#### Import and pass translator into your module
> **constructor(private _translate: TranslateService) {}**

####  Create a list of supportedLanguages ngOnInit
  > public ngOnInit() {
      // standing data
      this.supportedLanguages = [
	        { display: 'PT (Br)', value: 'ptbr' },
	        { display: 'English', value: 'en'},
	        { display: 'Español', value: 'es'},
      ];
      // set current langage
      this.selectLang('ptbr');
  }
  
####  Create a list of supportedLanguages ngOnInit
  > selectLang(lang: string) {
    // set current lang;
    this._translate.use(lang);
  }

  **Check if a language is current language** 
 > isCurrentLang(lang: string) {
   ** check if the selected lang is current lang**
    return lang === this._translate.currentLang;
  }

How use translate service in component.ts
===================
----------
#### Import translate service to your app.component.ts
> **import { TranslateService } from '../../../translate';**
>
#### Pass it in constructor parameter
> **constructor(private _translate: TranslateService) {}**
> 
####  Create a variable
  > public translatedText = '';
  > **Initialize variable in constructor**
     constructor(private _translate: TranslateService) {
	    this.translatedText = this._translate.instant('hello world');
  }
  
####  Create function to refresh text when language change
> refreshText() {
    this.translatedText = this._translate.instant('hello world');
  }
  subscribeToLangChanged() {
    return this._translate.onLangChanged.subscribe(x => this.refreshText());
  }
#### Initialize subsccrible in ngOnInit()
>ngOnInit() {
    this.subscribeToLangChanged();
  }
