// lang-en.ts

export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {
  'current language': 'English (USA)',
  'hello world': 'hello world',
  'Dashboard': 'Dashboard',
  'Historic': 'Historic'
};
