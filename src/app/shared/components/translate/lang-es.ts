// lang-es.ts

export const LANG_ES_NAME = 'es';

export const LANG_ES_TRANS = {
  'current language': 'Espanol (ES)',
  'Dashboard': 'Tablero',
  'Historic': 'Historia'
};
