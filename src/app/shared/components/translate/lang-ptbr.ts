// lang-ptbr.ts

export const LANG_PTBR_NAME = 'ptbr';

export const LANG_PTBR_TRANS = {
  'current language': 'Portugues (BR)',
  'Dashboard': 'Painel de controles',
  'Historic': 'Histórico'
};
