/**
 * Angular Dependency
 */
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

/**
 * Services
 */
import { ModalService } from '../modal/modal.service';

/**
 * Components
 */
import { ConfirmDialogComponent } from './confirm/confirm.dialog';

@Component({
  selector: 'app-dialog',
  template: '<h1>Confirm dialog</h1>'
})
export class DialogComponent implements OnInit {
  constructor(private modalService: ModalService) {}
  @Output('confirm') confirm = new EventEmitter<boolean>();
  @Output('cancel') cancel = new EventEmitter<boolean>();

  ngOnInit() {
    setTimeout(() => {
      this.initLoginModal();
    }, 5000);
  }

  initLoginModal() {
    const inputs = {
      isMobile: 'Apenas um teste de input'
    };
    const outputs = {
      close: this.close()

    };
    this.modalService.init(ConfirmDialogComponent, inputs, outputs);
  }
  public close() {
   //
  }
}
