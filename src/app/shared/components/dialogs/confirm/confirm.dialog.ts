/**
 * Angular Dependency
 */
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ModalService } from '../../modal/modal.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: 'confirm.dialog.html',
  styleUrls: ['./confirm.dialog.scss']
})
export class ConfirmDialogComponent implements OnInit {
  @Input() dialogTitle: string;
  @Input() dialogText: string;
  public cancelClick;
  public okButton;
  constructor(private modalService: ModalService) {}

  cancel() {
    this.modalService.destroy();
  }
  ngOnInit() {}
}
