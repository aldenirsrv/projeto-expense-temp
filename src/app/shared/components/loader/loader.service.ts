import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { LoaderState } from './loader';

@Injectable()
export class LoaderService {
  private loaderSubject = new Subject<LoaderState>();

  loaderState = this.loaderSubject.asObservable();

  constructor() {
    console.log(this.loaderState);
  }

  show() {
    this.loaderSubject.next(<LoaderState>{ show: true });
    document.getElementById('app-layout').className = 'modal-open-blured';
  }

  hide() {
    this.loaderSubject.next(<LoaderState>{ show: false });
    document.getElementById('app-layout').className = 'modal-close-blured';
  }
}
