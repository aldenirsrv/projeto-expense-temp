// https://www.toptal.com/angular-js/angular-4-forms-validation
import { Component, Input } from '@angular/core';
import {
  AbstractControlDirective,
  AbstractControl,
  FormControlDirective
} from '@angular/forms';

@Component({
  selector: 'app-errors',
  styleUrls: ['./error.component.scss'],
  template: `
    <ul *ngIf="showErrors()" class="error-list">
      <li *ngFor="let error of errors()">* {{error}}</li>
    </ul>
  `
})
export class ErrorsComponent {
  private static readonly errorMessages = {
    required: params => 'Campo de preenchimento obrigatório',
    minlength: params =>
      'The min number of characters is ' + params.requiredLength,
      maxlength: params =>
        'The max allowed number of characters is ' + params.requiredLength,
        min: params =>
          'The max allowed number of characters is ' + params.min,
    pattern: params => 'The required pattern is: ' + params.requiredPattern,
    age: params => params.message,
    email: params => params.message,
    validateEqual: params => 'Os campos não conferem!',
    years: params => params.message,
    minValue: params => params.message,
  };

  @Input()
  private control:
    | AbstractControlDirective
    | AbstractControl
    | FormControlDirective;

  showErrors(): boolean {
    return (
      this.control &&
      this.control.errors &&
      (this.control.dirty || this.control.touched)
    );
  }

  errors(): string[] {
    return Object.keys(this.control.errors).map(field =>
      this.getMessage(field, this.control.errors[field])
    );
  }

  private getMessage(type: string, params: any) {
    return ErrorsComponent.errorMessages[type](params);
  }
}
