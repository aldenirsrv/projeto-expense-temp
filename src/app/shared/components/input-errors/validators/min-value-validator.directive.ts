import { Directive, Input } from '@angular/core';
import {
  NG_VALIDATORS,
  FormControl,
  Validator,
  ValidationErrors,
} from '@angular/forms';

@Directive({
  selector: '[minValue]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: MinValidatorDirective,
      multi: true,
    },
  ],
})
export class MinValidatorDirective implements Validator {
  @Input('minValue') params: string;
  validate(c: FormControl): ValidationErrors {
    const minValue = Number(this.params);
    const numValue = Number(c.value);
    let isValid: boolean;

    if (!minValue) {
      isValid = true;
      return null;
    }

    isValid = minValue && numValue >= minValue;
    const message = {
      minValue: {
        message:
          `O valor deve ser maior que ${minValue ? minValue : 0}`
      },
    };
    return isValid ? null : message;
  }
}
