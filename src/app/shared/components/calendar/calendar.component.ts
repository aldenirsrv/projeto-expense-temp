// http://www.bentedder.com/create-calendar-grid-component-angular-4/
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';

export interface CalendarDate {
  mDate: moment.Moment;
  selected?: boolean;
  today?: boolean;
  events?: Array<any>;
}

export interface DateRange {
  dateStart: moment.Moment;
  dateEnd?: moment.Moment;
}
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnChanges {

  currentDate = moment();
  dayNames = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];
  weeks: CalendarDate[][] = [];
  sortedDates: CalendarDate[] = [];
  selectedDate;
  dayToselect;
  rangeDate: DateRange;
  isRanging: boolean;
  @Input() selectedDates: CalendarDate[] = [];
  @Input() cleaner: boolean;
  @Input() calendarSelected: boolean;
  @Output() onSelectDate = new EventEmitter<CalendarDate>();

  constructor() {
    this.rangeDate = {
      dateStart: null,
      dateEnd: null
    };
  }

  ngOnInit(): void {
    this.generateCalendar();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedDates &&
      changes.selectedDates.currentValue &&
      changes.selectedDates.currentValue.length > 1) {
      // sort on date changes for better performance when range checking
      this.sortedDates = _.sortBy(changes.selectedDates.currentValue, (m: CalendarDate) => m.mDate.valueOf());
      this.generateCalendar();
    }
    if (!changes.calendarSelected.currentValue) {

    }

    this.calendarCleaner(changes.calendarSelected.currentValue);

    // if(changes.cleaner && changes.cleaner.currentValue){}
    // this.calendarCleaner(changes.calendarSelected.currentValue)
    // this.doSomething(changes.calendarSelected.currentValue);
  }

  calendarCleaner = (b: boolean) => {
    if (b) { } else {
      this.selectedDate = null;
    }
  }
  // date checkers

  isToday(date: moment.Moment): boolean {
    return moment().isSame(moment(date), 'day');
  }

  isSelected(date: moment.Moment): boolean {
    return _.findIndex(this.selectedDates, (selectedDate) => {
      return moment(date).isSame(selectedDate.mDate, 'day');
    }) > -1;
  }
  isSelectedDay(date: moment.Moment): boolean {
    return moment().isSame(moment(date), 'day');
  }

  isSelectedMonth(date: moment.Moment): boolean {
    return moment(date).isSame(this.currentDate, 'month');
  }

  selectDate(date: CalendarDate): void {
    this.onSelectDate.emit(date);
    this.selectedDate = date.mDate.dayOfYear();
  }
  selectRange(date: moment.Moment) {
    if (!this.rangeDate.dateStart) {
      this.rangeDate.dateStart = date;
    } else {
      this.rangeDate.dateEnd = date;
    }
    this.selectedDate = date;
    if (!this.isRanging) {
      this.dayToselect = null;
      this.rangeDate.dateStart = date;
      this.rangeDate.dateEnd = null;
    }
    this.isRanging = this.rangeDate.dateStart && this.rangeDate.dateEnd ? false : true;
  }

  // actions from calendar

  prevMonth(): void {
    this.currentDate = moment(this.currentDate).subtract(1, 'months');
    this.generateCalendar();
  }

  nextMonth(): void {
    this.currentDate = moment(this.currentDate).add(1, 'months');
    this.generateCalendar();
  }

  firstMonth(): void {
    this.currentDate = moment(this.currentDate).startOf('year');
    this.generateCalendar();
  }

  lastMonth(): void {
    this.currentDate = moment(this.currentDate).endOf('year');
    this.generateCalendar();
  }
  isInRange = (date) => {
    if (this.isRanging) {
      this.dayToselect = moment(date);
    }

    this.isRanging = this.rangeDate.dateStart && this.rangeDate.dateEnd ? false : true;
    // console.log(date)
  }

  prevYear(): void {
    this.currentDate = moment(this.currentDate).subtract(1, 'year');
    this.generateCalendar();
  }

  nextYear(): void {
    this.currentDate = moment(this.currentDate).add(1, 'year');
    this.generateCalendar();
  }

  // generate the calendar grid

  generateCalendar(): void {
    const dates = this.fillDates(this.currentDate);
    // this.selectedDate = this.currentDate.dayOfYear()
    // alert(this.currentDate.dayOfYear())

    const weeks: CalendarDate[][] = [];
    while (dates.length > 0) {
      weeks.push(dates.splice(0, 7));
    }
    this.weeks = weeks;
  }

  fillDates(currentMoment: moment.Moment): CalendarDate[] {
    const firstOfMonth = moment(currentMoment).startOf('month').day();
    const firstDayOfGrid = moment(currentMoment).startOf('month').subtract(firstOfMonth, 'days');
    const start = firstDayOfGrid.date();
    return _.range(start, start + 42)
      .map((date: number): CalendarDate => {
        const d = moment(firstDayOfGrid).date(date);
        return {
          today: this.isToday(d),
          selected: this.isSelected(d),
          mDate: d,
          events: []
        };
      });
  }

}
