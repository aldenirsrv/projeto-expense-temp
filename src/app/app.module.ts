/**
 * Angular app dependency
 * https://medium.com/netscape/the-better-way-to-write-api-service-in-angular-4-c9178ecf7f48
 * https://hackernoon.com/best-practices-learnt-from-delivering-a-quality-angular4-application-2cd074ea53b3
 * https://cursos.alura.com.br/forum/topico-adicionando-token-em-toda-requisao-com-angular-2-solucao-27821
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaskModule } from 'soft-angular-mask';
/**
 * Shared components and services
 */
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { LayoutComponent } from './templates/admin/layout.component';
import { PublicComponent } from './templates/public/public.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { SiteTemplateComponent } from './templates/site/site-template.component';
import { LoaderService } from './shared/components/loader/loader.service';
import { TranslateService, TRANSLATION_PROVIDERS } from './shared/components/translate';
import * as moment from 'moment';
import 'hammerjs';
moment.locale('pt-br');
import * as $ from 'jquery';

/**
 * Thirty party
 */
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { environment } from '../environments/environment';
/**
 * Directives
 */

// import { HomeComponent } from './pages/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    LayoutComponent,
    PublicComponent,
    SiteTemplateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    SnotifyModule,
    SharedModule,
    MaskModule
  ],
  providers: [
    {
      provide: 'SnotifyToastConfig',
      useValue: ToastDefaults
    },
    SnotifyService,
    TRANSLATION_PROVIDERS, TranslateService,
    LoaderService],
  bootstrap: [AppComponent]
})
export class AppModule {
  static injector: Injector;
  constructor(public injector: Injector) {
    AppModule.injector = injector;
  }
}
