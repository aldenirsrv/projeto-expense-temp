
import { environment } from './../environments/environment';

export class AppSettings {
  // public static API_ENDPOINT = 'http://localhost:3000/api/';
  public static API_ENDPOINT = 'http://ec2-34-205-177-37.compute-1.amazonaws.com:3000/api/';
  // public static API_ENDPOINT = environment.APIEndpoint;
  public static accessTokenServer = 'Authorization';
  public static accessTokenLocalStorage = 'Authorization';
  public static defaultContentTypeHeader = 'application/json';
  public static LOGIN = 'usuarios/autenticar';
  public static userLocalStorage = 'user';
  public static loginPageUrl = '/login';
  /** CLIENT */
  public static LIST_CLIENT = 'clientes/listar';
  public static LIST_CLIENT_ID = 'clientes/listarCliente';
  public static UPDATE_CLIENT = 'clientes/atualizar';
  public static ADD_CLIENT = 'clientes/adicionar';

  /** USER */
  public static LIST_USER = 'usuarios/listar';
  public static LIST_USER_ID = 'usuarios/listarUsuario';
  public static UPDATE_USER = 'usuarios/atualizar';
  public static ADD_USER = 'usuarios/registrar';

  /** GROUP */
  public static LIST_GROUP = 'grupos/listar';
  public static LIST_GROUP_ID = 'grupos/listarGrupo';
  public static UPDATE_GROUP = 'grupos/atualizar';
  public static ADD_GROUP = 'grupos/registrar';
  public static GROUP_LIST_AVAILABLE_USER = 'grupos/listarUsuariosDisponiveis';
  public static GROUP_LIST_USERS = 'grupos/listarUsuarios';
  public static GROUP_ADD_USER = 'grupos/adicionarUsuario';
  public static GROUP_LIST_AVAILABLE_GROUPS = 'grupos/listarGruposDisponiveis';
  public static GROUP_LIST_GROUPS = 'grupos/listarRelacionamentos';
  public static GROUP_ADD_GROUP = 'grupos/adicionarUsuario';

  /** PROCESSOS */
  public static LIST_PROCCESSE = 'processos/listar';
  /** PROCESSOS */
  public static LIST_PROCCESSE_ID = 'processos/recuperarPorId';
}
/**
 * Bancos RDS
 * Instance: dev-db
 * Endpoint: dev-db.c7tvhzzh2lrs.us-east-1.rds.amazonaws.com
 * DB: devDBLawApp
 * User: lawAppDBAdmin
 * Pass: hVQj_Z9mT5JFbgJR
 **/
