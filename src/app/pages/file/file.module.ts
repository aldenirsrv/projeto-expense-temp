import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileRoutingModule } from './file-routing.module';
import { FileComponent } from './file.component';
import { FileDetailComponent } from './detail/file-detail.component';
import { FormComponent } from './form/form.component';



/**
 * Shared components and services
 */
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FileRoutingModule,
  ],
  declarations: [FileComponent, FileDetailComponent, FormComponent]
})
export class FileModule { }
