/*** Angular imports  */
import { Component, OnInit } from '@angular/core';
import {
  SnotifyService,
  SnotifyPosition,
  SnotifyToastConfig
} from 'ng-snotify';

import { LoaderService } from '../../shared/components/loader/loader.service';
/***  Interfaces imports */

@Component({
  template: `
  <div class="w-100p h-100p peers ai-c jc-c">

    <router-outlet  #route="outlet"></router-outlet>
  </div>`
})
export class FileComponent implements OnInit {
  constructor(
    private snotifyService: SnotifyService,
    private loaderService: LoaderService
  ) {}
  public toastConfig = {
    title: '',
    body: '',
    bodyMaxLength: 0
  };
  ngOnInit() {}

  onError = () => {
    this.snotifyService.error('Tem algo errado!', 'Erro', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true
    });
    console.log(this.toastConfig);
  }

  onSuccess = () => {
    this.snotifyService.success(this.toastConfig.title, 'Sucesso!', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true
    });
    console.log(this.toastConfig);
  }
  openLoader = () => {
    this.loaderService.show();
    setTimeout(() => {
      this.loaderService.hide();
      this.onError();
    }, 3000);
  }
}
