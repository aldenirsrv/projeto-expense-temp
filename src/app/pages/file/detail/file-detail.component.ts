import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Router, ActivatedRoute } from '@angular/router';
import { BaseService } from '../../../../singleton/http/base.service';
import { AppSettings } from '../../../app.constants';


@Component({
  selector: 'app-file-detail',
  templateUrl: './file-detail.component.html',
  styleUrls: ['./file-detail.component.scss']
})
export class FileDetailComponent implements OnInit {
  hasAppeared: boolean;
    /** url param */
    public activeParam: string;
  constructor( private router: Router,
    private route: ActivatedRoute,  public http: BaseService) {

    }
  // subscribe to subjects
  ngOnInit() {
    this.getParam();
  }

  /**
   * Get URL atribute id
   */
  getParam = () => {
    return this.route.params.subscribe(params => {
      const mailParam = params['id'];
      this.activeParam = mailParam;
      if (mailParam === '') {
        this.goBack();
        return;
      }
      this.groupList(mailParam);
    });
  }
    /**
   * Go back to group list
   */
  goBack = () => {
    this.router.navigate(['/']);
  }


  /**
   * Request to endpoint load a client by idClient
   * @param idUsuario
   */
  groupList = async (idUsuario: string) => {
    /** Param to endpoint */
    const params = {
      idUsuario: idUsuario
    };
    /** Post request do endpoint (http -> BaseService) */
    return await this.http
      .post(AppSettings.API_ENDPOINT + AppSettings.LIST_GROUP_ID, params)
      .subscribe(
        async (res: any) => {
          return res;
        },
        (error: Response) => Observable.throw(error)
      );
  }
}
