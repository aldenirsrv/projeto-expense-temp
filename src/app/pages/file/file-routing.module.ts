import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FileComponent } from './file.component';
import { FileDetailComponent } from './detail/file-detail.component';
import { FormComponent } from './form/form.component';


export const routes: Routes = [
  {
    path: '',
    component: FileComponent,
    children: [
      {
        path: 'file/:id',
        component: FileDetailComponent
      },
      {
        path: 'form',
        component: FormComponent,
        pathMatch: 'full',
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
  ]
})
export class FileRoutingModule { }
