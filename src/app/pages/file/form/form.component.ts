import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../../../singleton/http/base.service';
import { AppSettings } from '../../../app.constants';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  hasAppeared: boolean;
    /** url param */
    public activeParam: string;
  constructor(public http: BaseService) {

    }
  // subscribe to subjects
  ngOnInit() {}




  /**
   * Request to endpoint load a client by idClient
   * @param idUsuario
   */
  groupList = async (idUsuario: string) => {
    /** Param to endpoint */
    const params = {
      idUsuario: idUsuario
    };
    /** Post request do endpoint (http -> BaseService) */
    return await this.http
      .post(AppSettings.API_ENDPOINT + AppSettings.LIST_GROUP_ID, params)
      .subscribe(
        async (res: any) => {
          return res;
        },
        (error: Response) => Observable.throw(error)
      );
  }
}
