import { Component, OnInit } from '@angular/core';

@Component({
  template: `<router-outlet  #route="outlet"></router-outlet>`,
  styleUrls: ['./billings.component.scss']
})
export class BillingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
