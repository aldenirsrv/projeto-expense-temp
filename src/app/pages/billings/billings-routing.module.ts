import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingsComponent } from './billings.component';
import { BillingsDetailsComponent } from './billings-details/billings-details.component';


export const routes: Routes = [
  {
    path: '',
    component: BillingsComponent,
    children: [
      {
        path: '',
        component: BillingsDetailsComponent,
        pathMatch: 'full'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingsRoutingModule { }
