import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingsRoutingModule } from './billings-routing.module';
import { BillingsComponent } from './billings.component';
import { BillingsDetailsComponent } from './billings-details/billings-details.component';

import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BillingsRoutingModule
  ],
  declarations: [BillingsComponent, BillingsDetailsComponent]
})
export class BillingsModule { }
