import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApprovalsRoutingModule } from './approvals-routing.module';
import { ApprovalsComponent } from './approvals.component';
import { ApprovalsListComponent } from './approvals-list/approvals-list.component';
import { ApprovalsDetailsComponent } from './approvals-details/approvals-details.component';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApprovalsRoutingModule
  ],
  declarations: [ApprovalsComponent, ApprovalsListComponent, ApprovalsDetailsComponent]
})
export class ApprovalsModule { }
