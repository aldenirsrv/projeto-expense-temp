import { Component, OnInit } from '@angular/core';

@Component({
  template: `<router-outlet  #route="outlet"></router-outlet>`
})
export class ApprovalsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
