import { ApprovalsModule } from './approvals.module';

describe('ApprovalsModule', () => {
  let approvalsModule: ApprovalsModule;

  beforeEach(() => {
    approvalsModule = new ApprovalsModule();
  });

  it('should create an instance', () => {
    expect(approvalsModule).toBeTruthy();
  });
});
