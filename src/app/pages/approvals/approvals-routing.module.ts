import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApprovalsComponent } from './approvals.component';
import { ApprovalsDetailsComponent } from './approvals-details/approvals-details.component';
import { ApprovalsListComponent } from './approvals-list/approvals-list.component';


export const routes: Routes = [
  {
    path: '',
    component: ApprovalsComponent,
    children: [
      {
        path: '',
        component: ApprovalsListComponent,
      },
      {
        path: ':id',
        component: ApprovalsDetailsComponent
      },
      {
        path: 'new',
        component: ApprovalsDetailsComponent,
        pathMatch: 'full',
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalsRoutingModule { }
