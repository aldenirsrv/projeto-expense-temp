import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../../shared/components/sidebar/sidebar.service';
SidebarService

@Component({
  selector: 'app-users-filter',
  templateUrl: './users-filter.component.html',
  styleUrls: ['./users-filter.component.scss']
})
export class UsersFilterComponent implements OnInit {

  constructor(private sidebar: SidebarService) { }

  ngOnInit() {
  }
  cancel() {
    this.sidebar.destroy();
  }
}
