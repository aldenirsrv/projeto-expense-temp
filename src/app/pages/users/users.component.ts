import { Component, OnInit } from '@angular/core';

@Component({
  template: `<router-outlet  #route="outlet"></router-outlet>`
})
export class UsersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
