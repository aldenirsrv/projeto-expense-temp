import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SidebarService } from '../../../shared/components/sidebar/sidebar.service';
import { UsersFilterComponent } from '../users-filter/users-filter.component';
import { ModalService } from '../../../shared/components/modal/modal.service';
import { ConfirmDialogComponent } from '../../../shared/components/dialogs/confirm/confirm.dialog';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  constructor(private sidebar: SidebarService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: ModalService) { }
    public showSidenav: boolean;

  ngOnInit() {
  }
  confirm(data) {
    const inputs = {
      dialogTitle: '<div class="client-default ta-c c-red-500"><b>Tem certeza que deseja excluir?</b></div>',
      dialodObject: data,
      // tslint:disable-next-line:max-line-length
      dialogText: `<div class="ta-c"><span class="client-default">Você tem certeza que deseja excluir o arquivo<b> ${data.titulo} </b>?</span><span class="c-client-secondary fsz-xs"> Essa operação não poderá ser desfeita.</span></div>`
    };
    const outputs = {
      cancelButton: (e) => {
        alert('Cancel');
      },
      okButton: (e) => {
        alert('ok');
      }
    };

    // this.sidebar.init(MensagemDetalhesComponent, inputs, outputs, 'center');
    this.sidebar.init(UsersFilterComponent, inputs, outputs);
  }

  open(id) {
    this.router.navigate(['./', id], { relativeTo: this.route });
    /*this.routerExtensions.navigate(['edit',id], {
            transition: {
                name: 'fade'
            }
        });*/
  }
  openSideNav = () => {
    this.showSidenav = true;
  }
  closeSidenav = (event) => {
    this.showSidenav = event;
  }
  initLoginModal() {

    const inputs = {
      dialogTitle: '<div class="c-red-700"><b>Atenção!</b></div>',
      // tslint:disable-next-line:max-line-length
      dialogText: `<div class="c-grey-700">Você tem certeza que deseja excluir o usuário (Aldenir)?</div><i><sub>*Esta operação não poderá ser desfeita</sub></i>`
    };
    const outputs = {
      cancelClick: e => {
        console.log(e);
      },
      okButton: e => {
        console.log('Usuário pode salvar os dados');
      }
    };
    this.modalService.init(ConfirmDialogComponent, inputs, outputs, 'center');
  }


}
