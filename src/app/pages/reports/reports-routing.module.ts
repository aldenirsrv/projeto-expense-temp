import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { ReportsDetailsComponent } from './reports-details/reports-details.component';
import { ReportsListComponent } from './reports-list/reports-list.component';


export const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    children: [
      {
        path: '',
        component: ReportsListComponent,
      },
      {
        path: ':id',
        component: ReportsDetailsComponent
      },
      {
        path: 'new',
        component: ReportsDetailsComponent,
        pathMatch: 'full',
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
