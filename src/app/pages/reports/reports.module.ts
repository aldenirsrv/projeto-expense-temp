import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsListComponent } from './reports-list/reports-list.component';
import { ReportsDetailsComponent } from './reports-details/reports-details.component';
import { ReportsComponent } from './reports.component';


import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReportsRoutingModule
  ],
  declarations: [ReportsComponent, ReportsListComponent, ReportsDetailsComponent]
})
export class ReportsModule { }
