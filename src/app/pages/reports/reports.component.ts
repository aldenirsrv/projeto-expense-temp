import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  template: `<router-outlet  #route="outlet"></router-outlet>`
})
export class ReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
