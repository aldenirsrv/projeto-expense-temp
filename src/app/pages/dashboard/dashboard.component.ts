import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public data = [
    {
      name: '',
     data: [100, 90, 30, 105, 50, 150, 100, 30, 75]
   }];
   public billings = [
     // tslint:disable-next-line:max-line-length
     {month: 'Setembro', status: 'AB', value: 'R$ 100,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Agosto', status: 'PA', value: 'R$ 90,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Julho', status: 'PA', value: 'R$ 30,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Junho', status: 'PA', value: 'R$ 105,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Maio', status: 'PA', value: 'R$ 50,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Abril', status: 'PA', value: 'R$ 150,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Março', status: 'PA', value: 'R$ 100,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Fevereiro', status: 'PA', value: 'R$ 30,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
     // tslint:disable-next-line:max-line-length
     {month: 'Janeiro', status: 'PA', value: 'R$ 75,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    ];
   panelOpenState = false;
  constructor() { }

  ngOnInit() {
  }

}
