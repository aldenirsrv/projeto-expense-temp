import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnswerRoutingModule } from './answer-routing.module';
import { AnswerComponent } from './answer.component';
import { AnswerDetailsComponent } from './answer-details/answer-details.component';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AnswerRoutingModule
  ],
  declarations: [AnswerComponent, AnswerDetailsComponent]
})
export class AnswerModule { }
