import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnswerDetailsComponent } from './answer-details/answer-details.component';
import { AnswerComponent } from './answer.component';
export const routes: Routes = [
  {
    path: '',
    component: AnswerComponent,
    children: [
      {
        path: '',
        component: AnswerDetailsComponent,
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnswerRoutingModule { }
