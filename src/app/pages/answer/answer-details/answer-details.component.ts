import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-answer-details',
  templateUrl: './answer-details.component.html',
  styleUrls: ['./answer-details.component.scss']
})
export class AnswerDetailsComponent implements OnInit {
  public billings = [
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'AB', value: 'R$ 100,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 90,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 30,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 105,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 50,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 150,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 100,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 30,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 75,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'AB', value: 'R$ 100,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 90,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 30,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 105,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 50,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 150,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 100,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 30,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
    // tslint:disable-next-line:max-line-length
    {title: 'Corrigir um pagamento recusado', status: 'PA', value: 'R$ 75,00', details: 'Lorem ipsum dolor sit amet, deseruisse efficiendi an duo, sumo euismod fabulas eos ei. Te reque admodum vim. Sit id labore voluptua vituperata. No vel cibo illum, adipisci mediocrem quo ut. Usu congue deserunt patrioque ea' },
   ];
  panelOpenState = false;
  constructor() { }

  ngOnInit() {
  }

}
