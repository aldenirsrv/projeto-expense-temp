import { Component, OnInit } from '@angular/core';

@Component({
  template: `<div id="login-component">
                <router-outlet  #route="outlet"></router-outlet>
            </div>`,
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
