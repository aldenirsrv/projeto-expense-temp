import { Component, OnInit } from '@angular/core';

@Component({
  template: `<router-outlet  #route="outlet"></router-outlet>`,
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
