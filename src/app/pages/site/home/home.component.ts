import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  hasAppeared: boolean;
  constructor() { }

  ngOnInit() {
  }

  onAppear(e) {
    this.hasAppeared = true;
    console.log(e);
  }

}
