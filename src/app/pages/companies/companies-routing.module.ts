import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesComponent } from './companies.component';
import { CompaniesDetailsComponent } from './companies-details/companies-details.component';
import { CompaniesListComponent } from './companies-list/companies-list.component';

export const routes: Routes = [
  {
    path: '',
    component: CompaniesComponent,
    children: [
      {
        path: '',
        component: CompaniesListComponent,
      },
      {
        path: ':id',
        component: CompaniesDetailsComponent
      },
      {
        path: 'new',
        component: CompaniesDetailsComponent,
        pathMatch: 'full',
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompaniesRoutingModule { }
