import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesListComponent } from './companies-list/companies-list.component';
import { CompaniesDetailsComponent } from './companies-details/companies-details.component';
import { CompaniesComponent } from './companies.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CompaniesRoutingModule
  ],
  declarations: [CompaniesListComponent, CompaniesDetailsComponent, CompaniesComponent]
})
export class CompaniesModule { }
