import {  NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './templates/admin/layout.component';
import { PublicComponent } from './templates/public/public.component';
import { SiteTemplateComponent } from './templates/site/site-template.component';

import { LoggedInGuard } from '../singleton/guard/loggedIn-guard.service';


/**
 * App components
 */

export const routes: Routes = [
  {
    path: '', // {1}
    // component: LayoutComponent,
    // canActivate: [AuthGuard],       // {2}
    children: [
      {
        path: 'admin',
        component: LayoutComponent,
        children: [
          {
           path: '',
           loadChildren: '../app/pages/dashboard/dashboard.module#DashboardModule',
           pathMatch: 'full',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'dashboard',
           loadChildren: '../app/pages/dashboard/dashboard.module#DashboardModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'users',
           loadChildren: '../app/pages/users/users.module#UsersModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'companies',
           loadChildren: '../app/pages/companies/companies.module#CompaniesModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'approvals',
           loadChildren: '../app/pages/approvals/approvals.module#ApprovalsModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'billings',
           loadChildren: '../app/pages/billings/billings.module#BillingsModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'reports',
           loadChildren: '../app/pages/reports/reports.module#ReportsModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'categories',
           loadChildren: '../app/pages/categories/categories.module#CategoriesModule',
            // canActivate: [LoggedInGuard]
          },
          {
           path: 'profile',
           loadChildren: '../app/pages/profile/profile.module#ProfileModule',
            // canActivate: [LoggedInGuard]
          }
        ]
      },
      // { path: '**',  loadChildren: '../app/pages/login/login.module#LoginModule'},
      {
        path: 'public',
        component: PublicComponent,
        children: [
          {
            path: '',
            loadChildren:  '../app/pages/login/login.module#LoginModule',
            // pathMatch: 'full',
             // canActivate: [LoggedInGuard]
           },
           {
             path: 'answer',
             loadChildren:  '../app/pages/answer/answer.module#AnswerModule',
             // pathMatch: 'full',
              // canActivate: [LoggedInGuard]
            },
        ]
      },
      {
        path: '',
        component: SiteTemplateComponent,
        children: [
          {
            path: '',
            loadChildren:  '../app/pages/site/site.module#SiteModule',
            // pathMatch: 'full',
             // canActivate: [LoggedInGuard]
           }
        ]
      }/*,
      {
        path: 'signup',
        loadChildren: '../app/pages/signup/signup.module#SignupModule',
        data: { state: 'signup' }
      }*/
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing: true, useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
