import { Injectable } from '@angular/core';
import { Response, ResponseOptions } from '@angular/http';


import { Success } from '../interfaces/success.interface';
import { TostNotificationService } from '../notification/toast-notification.service';


@Injectable()
export class CustomSuccessHandlerService {
  constructor(public snotify: TostNotificationService) { }


  tryParseSuccess(success: Response): any {
    try {
      return success.json().success;
    } catch (ex) {
      try {
        return success;
      } catch (ex) {
        return success.toString();
      }
    }
  }


  parseCustomServerSuccess(sucsess: Success): any {
    const title = sucsess.message;
    let body = '';
    for (const sucsessMsg of sucsess.message) {
      body += `${sucsessMsg}. `;
    }


    return { title, body };
  }


  createCustomSuccess(success: Success): Response {
    try {
      const parsedSuccess = this.parseCustomServerSuccess(success);
      const responseOptions = new ResponseOptions({
        body: { success: { title: parsedSuccess.title, message: parsedSuccess.body } },
        status: 200,
        headers: null,
        url: null,
      });
      return new Response(responseOptions);
    } catch (ex) {
      const responseOptions = new ResponseOptions({
        body: { title: 'Unknown success!', message: 'Unknown success Occurred.' },
        status: 200,
        headers: null,
        url: null,
      });
      return new Response(responseOptions);
    }
  }


  showToast(success: Success): void {
    const parsedSuccess = this.parseCustomServerSuccess(success);
    this.snotify.toastConfig.title = parsedSuccess.title;
    this.snotify.toastConfig.body = parsedSuccess.body;
    this.snotify.toastConfig.bodyMaxLength = 100;
    this.snotify.onSuccess();
  }


  parseCustomServerSuccessToString(success: Success): string {
  this.showToast(success);
    const parsedSuccess = this.createCustomSuccess(success);
    try {
      return JSON.stringify(this.tryParseSuccess(parsedSuccess));
    } catch (ex) {
      try {
        return success.toString();
      } catch (error) {
        return error.toString();
      }
    }
  }
}
