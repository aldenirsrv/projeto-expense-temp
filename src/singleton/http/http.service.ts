import { Injectable } from '@angular/core';
import {
  Http,
  XHRBackend,
  RequestOptions,
  Request,
  RequestOptionsArgs,
  Response,
  Headers
} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Router } from '@angular/router';

import { AppModule } from '../../app/app.module';
import { AppSettings } from '../../app/app.constants';
import { HelperService } from './helper.service';
import { Error } from '../interfaces/error.interface';
import { CustomErrorHandlerService } from './custom-error-handler.service';

@Injectable()
export class HttpService extends Http {
  helperService: HelperService;
  router: Router;

  constructor(
    backend: XHRBackend,
    options: RequestOptions,
    public errorHandler: CustomErrorHandlerService
  ) {
    super(backend, options);
    this.helperService = AppModule.injector.get(HelperService);
    const token = localStorage.getItem(AppSettings.accessTokenLocalStorage); // your custom token getter function here

    options.headers.set('Authorization', `Bearer ${token}`);
  }

  request(
    url: string | Request,
    options?: RequestOptionsArgs
  ): Observable<Response> {
    if (typeof url === 'string') {
      // meaning we have to add the token to the options, not in url
      if (!options) {
        // let's make option object
        options = { headers: new Headers() };
      }
      // options.headers.append('Authorization', `Bearer ${token}`);
      this.createRequestOptions(options);
    } else {
      // we have to add the token to the url object
      //  url.headers.append('Authorization', `Bearer ${token}`);
      this.createRequestOptions(url);
    }
    return super.request(url, options).catch(this.catchAuthError(this));
  }
  createRequestOptions(options: RequestOptionsArgs | Request) {
    const token: string = localStorage.getItem(
      AppSettings.accessTokenLocalStorage
    );
    if (
      this.helperService.addContentTypeHeader &&
      typeof this.helperService.addContentTypeHeader === 'string'
    ) {
      options.headers.set(
        'Content-Type',
        this.helperService.addContentTypeHeader
      );
    } else {
      const contentTypeHeader: string = options.headers.get('Content-Type');
      if (!contentTypeHeader && this.helperService.addContentTypeHeader) {
        options.headers.set(
          'Content-Type',
          AppSettings.defaultContentTypeHeader
        );
      }
      options.headers.set('Authorization', token);
    }
  }
  private catchAuthError(self: HttpService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      const data = res.json();
      if (res.status === 401 || res.status === 403) {
        // if not authenticated

        localStorage.removeItem(AppSettings.userLocalStorage);
        localStorage.removeItem(AppSettings.accessTokenLocalStorage);
        this.router.navigate([AppSettings.loginPageUrl]);

        console.log(res);
      }
      const error: Error = { status: data.status, message: data.message };
      this.errorHandler.createCustomError(error);
      // throw new Error(this.errorHandler.parseCustomServerErrorToString(error));

      return Observable.throw(res);
    };
  }
}
