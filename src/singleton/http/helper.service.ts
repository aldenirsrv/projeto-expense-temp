import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import {
  Headers,
  Http,
  Request,
  RequestOptions,
  Response,
  XHRBackend
} from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HelperService {
  public addContentTypeHeader = 'application/json';
  constructor() {}

  startLoader = () => {

  }

  stopLoader = () => {
   // this.slimLoadingBarService.stop();
  // this.slimLoadingBarService.complete();

  }
}
