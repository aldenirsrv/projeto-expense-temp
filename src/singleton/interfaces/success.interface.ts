export interface Success {
  status: string;
  message: string;
}
