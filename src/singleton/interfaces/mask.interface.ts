// https://github.com/renatosistemasvc/soft-angular-mask

/**
 * http://www.agiratech.com/angular-4-template-driven-forms-building-validating/
 */

/**
 * Data table model
 */


export class Mask {
  public static  CNPJ = [ /\d/ , /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/ , /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/] ;
  public static PHONE = ['(', /[1-9]/, /\d/, ')', ' ', /\d/ , /\d/ , /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
}
