import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
/** Thirty party */
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


import { Injectable } from '@angular/core';
import {
  Headers,
  Http,
  Request,
  RequestOptions,
  Response,
  XHRBackend
} from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TostNotificationService {
  public toastConfig = {
    title: '',
    body: '',
    bodyMaxLength: 0,
  };
  constructor(private snotifyService: SnotifyService) {}

    /*
  Change global configuration

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: this.backdrop,
      position: this.position,
      timeout: this.timeout,
      showProgressBar: this.progressBar,
      closeOnClick: this.closeClick,
      pauseOnHover: this.pauseHover
    };
  }*/

  onError = () => {
    this.snotifyService.error(this.toastConfig.title, 'Erro', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true
    });
    console.log(this.toastConfig);
  }

  onSuccess = () => {
    this.snotifyService.success(this.toastConfig.title, 'Sucesso!', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true
    });
    console.log(this.toastConfig);
  }
}
