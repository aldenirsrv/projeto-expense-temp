import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { AppSettings } from '../../app/app.constants';

import { BaseService } from '../http/base.service';

import { Router } from '@angular/router';

@Injectable()
export class UserService {

  private _loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public loggedIn: Observable<boolean> = this._loggedIn.asObservable();

  constructor(private http: BaseService, private router: Router) {}

  autentica(usuario: any) {
    return this.http.post(AppSettings.API_ENDPOINT + AppSettings.LOGIN, usuario).subscribe(res => {
      const token: any = res || null;
      if (token) {
        this._loggedIn.next(true);
        localStorage.setItem(AppSettings.accessTokenLocalStorage, `${token.token}`);
        this.router.navigate(['/']);
      }
    });
  }

  logout() {
    localStorage.removeItem(AppSettings.userLocalStorage);
    localStorage.removeItem(AppSettings.accessTokenLocalStorage);
    this.router.navigate(['/login']);
  }

  isLoggedIn() {
    const token = localStorage.getItem(AppSettings.accessTokenLocalStorage);

    if (token) {
      // essa atribuição é feita para atualizar a variavel e o resto do sistema ser notificado
      this._loggedIn.next(true);
    } else {
      this._loggedIn.next(false);
    }

    return this._loggedIn.getValue();
  }
}
